#![warn(clippy::all)]
#![warn(clippy::pedantic)]

#[macro_export]
macro_rules! path {
	($($e:expr),*) => {
		{
			let mut path = std::path::PathBuf::new();
			$(
				path.push($e);
			)*
			path
		}
	};
}

#[cfg(test)]
mod tests {
	#[test]
	fn test() {
		assert_eq!(
			std::path::Path::new("test/dummy/a123/").to_owned(),
			path!("test/dummy/a123")
		);

		assert_eq!(
			std::path::Path::new("test/dummy/a123/").to_owned(),
			path!("test", "dummy", "a123")
		);

		assert_eq!(
			std::path::Path::new("test/dummy/a123/").to_owned(),
			path!("test", std::path::Path::new("dummy"), "a123")
		);

		assert_eq!(
			std::path::Path::new("test/dummy/a123/").to_owned(),
			path!("test", std::path::PathBuf::from("dummy"), "a123")
		);
	}
}
